
// Import RSA package.
import Rsa.*;

// Import JAVA math.
import java.math.*;

// Import java utilities.
import java.util.*;

/**
 * Application main class.
 */
public class App {

	/**
	 * Main.
	 *
	 * Here we're going to generate the public keys.
	 */
	public static void main(String [] args) {

		// Create an RSA instance.
		Rsa rsa = new Rsa();

		// 1.1 Get two prime numbers.
		//
		// `Rsa` is a class that has helpers for
		// encrypting messages.
		//
		int p = Helper.getPrime();
		int q = Helper.getPrime();
		int e = 1;

		// 1.2 Calculate `n`.
		int n = p * q;

		// 1.3 Calculate `z`.
		int z = (p - 1) * (q - 1);

		// 1.4 Calculate coprime.
		for (int i = z; i > 0; i--)

			// Get coprime down below `z`.
			if (Helper.isCoPrime(i, z)) {
				e = i;
				break;
			}

		// Now we have our keys.
		System.out.println("Keys are:");
		System.out.println("n: " + n);
		System.out.println("e: " + e);

	}


}


