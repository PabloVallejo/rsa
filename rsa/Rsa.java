
// RSA Package.
package Rsa;

// Import utilities.
import java.util.*;

/**
 * Application main class.
 */
public class Rsa {

	/**
	 *  Public key A.
	 */
	public int publicKeyA;

	/**
	 * Public key B.
	 */
	public int publicKeyB;

	/**
	 * Private key A.
	 *
	 * For testing purposes we'll have it as
	 * a public variable.
	 */
	public int privateKeyA;

	/**
	 * Private key B.
	 *
	 * For testing purposes we'll have it as
	 * a public variable.
	 */
	public int privateKeyB;


	/**
	 * Constructor.
	 */
	public Rsa() {}


}


