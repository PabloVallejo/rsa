
// Rsa package.
package Rsa;

// Java utilities.
import java.util.*;

// Import math module.
import java.math.*;

/**
 * Helper class.
 */
public class Helper {

	/**
	 * Constructor
	 */
	public Helper() {}


	/**
	 * Get a prime number.
	 */
	public static int getPrime() {

		// Collection of prime numbers.
		int[] primes = {1, 2, 3, 5, 7, 11, 13, 17, 23, 29, 31, 37, 41, 43};

		// Create randon instance.
		Random r = new Random();

		// Get a random index.
		int index = r.nextInt(primes.length);

		// Return such prime number.
		return primes[index];
	}


	/**
	 * Checks whether a number is prime.
	 */
	public static boolean isPrime(int n) {

    // Check if n is a multiple of 2
    if (n % 2 == 0) return false;

    // if not, then just check the odds.
    for (int i = 3; i * i <= n; i += 2)

        if (n % i == 0)
            return false;

    return true;
	}

	/**
	 * Check whether two numbers are coprime.
	 *
	 * Coprimes are numbres that share no common denominators
	 * other than 1.
	 */
	public static boolean isCoPrime(int a, int b) {

		// Cycle from 2 until the smaller numbers of a and b.
		for (int i = 2; i <= Math.min(a, b); i++)

			// If both modulus with `i` are equal to `0`
			// the, the numbers are not coprime.
			if (a % i == b % i && a % i == 0)
				return false;

		// If we're here, both numbers are coprime.
		return true;

	}

}
